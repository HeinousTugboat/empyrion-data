# Empyrion Galactic Survival Configuration Data

This is just a repository of the raw data from EGS. Data is owned by the respective parties, and I do my best to keep this as up to date as possible.

Project Eden data hosted with permission from Ravien.
